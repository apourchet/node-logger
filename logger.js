var logger = exports = module.exports;
var fs = require('fs');

// Set defaults
logger._started = false;
logger._logs = {};
logger._dumpInterval = undefined;
logger._writeInterval = 1000;
logger._maxBufferSize = 10; // Max number of lines held by the buffer

logger.setWriteInterval = function(intervalMs) {
	this._writeInterval = intervalMs;
	this.restart();
}

logger.setMaxBufferSize = function(maxBufferSize) {
	this._maxBufferSize = maxBufferSize;
}

logger.newLog = function(key, outputFile, maxLines) {
	this._logs[key] = {
		outputFile: outputFile,
		outputBuffer: new Array(),
		maxLogLines: maxLines,
		currentLogLines: 0
	};
	console.log("Logging: " + key + " --> " + outputFile);
}

logger.restart = function() {
	if (this._started) {
		this.stop();
	}
	this.start();
}

logger.start = function() {
	this._dumpInterval = setInterval(this.dumpAll.bind(this), this._writeInterval);
	this._started = true;
}

logger.stop = function() {
	if (this._dumpInterval) {
		clearInterval(this._dumpInterval);
		this._dumpInterval = undefined;
	}
	this._started = false;
}

logger.finish = function() {
	this.stop();
	this.dumpAll();
}

logger.dumpAll = function() {
	for (var key in this._logs) {
		this.dump(key);
	}
}

logger.log = function(key, line) {
	var log = this._logs[key];

	if (log.outputBuffer.length >= this._maxBufferSize) {
		logger.dump(key)
	}

	log.outputBuffer.push(line);
	log.currentLogLines++;
}

logger.dump = function(key) {
	var log = this._logs[key];
	if (log.outputBuffer.length == 0) {
		return;
	}	

	if (log.currentLogLines <= log.maxLogLines) {
		fs.appendFileSync(log.outputFile, log.outputBuffer.join("\n") + "\n");
	} else { 
		if (log.outputBuffer.length > log.maxLogLines) {
			log.outputBuffer = log.outputBuffer.splice(log.outputBuffer.length - log.maxLogLines);
		}
		var previousLines = fs.readFileSync(log.outputFile) + "";
		previousLines = previousLines.split("\n")
		previousLines = previousLines.splice(log.outputBuffer.length + previousLines.length - log.maxLogLines - 1);
		fs.writeFileSync(log.outputFile, previousLines.join("\n") + log.outputBuffer.join("\n") + "\n");
	}
	log.outputBuffer = new Array();
}
