var log = require("../logger.js");
var logName = "test";


// We do not want our log file to go beyond 13 lines total
log.newLog(logName, "log.log", 13);

// Start the periodic dumping of the log buffers to the log files
log.start();

// Suppose that we do not want our buffers to go beyond 3000 lines, for memory reasons
log.setMaxBufferSize(3000);

// We want to make sure that the log file is updated to the latest every 10000 ms
// if the buffers didn't fill up prior to that
log.setWriteInterval(10000);

var writeLine = function(i) {
	log.log(logName, i + " log line!");
	if (i == 51) {
		log.finish();
	}
}

for (var i = 1; i <= 51; i++) {
	setTimeout(writeLine.bind(undefined, i), i * 20);
}

// We should get lines 39 to 51
